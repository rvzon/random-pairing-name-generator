package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/0xAX/notificator"
)

var notify *notificator.Notificator
var names string

func main() {

	rand.Seed(time.Now().UTC().UnixNano())
	answers := []string{
		"Ronald",
		"Vlad B",
		"Vlad S",
		"Alin",
		"Catalin",
		"Rene",
		"Rehab",
		"David W",
		"Silvester W",
		"Collen",
		"Muhamed",
		"Chris",
		"Ben",
		"Nourdin",
		"Aakif",
		"Donique",
		"Artur",
	}

	for i := 1; i < 4; i++ {
		randName := answers[rand.Intn(len(answers))]
		if strings.Contains(names, randName) {
			i--
		} else {
			names += fmt.Sprintf("%s|", randName)
		}
	}
	fmt.Println("Pairing sessions for this week:", names)

	notify = notificator.New(notificator.Options{
		DefaultIcon: "icon/golang.png",
		AppName:     "Rnadom pairing session generator",
	})

	notify.Push("Support Agents", names, "icon/golang.png", notificator.UR_CRITICAL)
}
